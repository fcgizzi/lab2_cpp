# Laboratorio 2 Unidad 1 - Pilas y Colas (Programa Directotio de Pila)


## Intro
El programa consiste en un directorio que trata el ingreso, el borrado y el muestreo de datos que contenga un pila a la que se le limita con un tamaño ingresado con anterioridad.

## Compilación
Para compilar se hace desde la terminal de linux utiliando el comando `g++ programa.cpp Pila.cpp -o programa`, o directamente con el comando `make`.

## Ejecutado y uso
Se inicia con el comando `./programa`, este abirirá y solicitará el tamaño máximo que tendrá la pila con la que se trabajará.
Posteriormente saltará un menú con cuatro opciones:

    Agregar/push
    Remover/pop
    Ver pila
    Salir

Al escoger una de estas, desencadenará su correspondiente función

## Requisitos
Sistema operativo compatible con c++
Se necesitará make para compilar de manera más sencilla

# Programa contruido con:
    - Linux
    - Geany
    - Lenguaje c++

## Autor
- Franco Cifuentes Gizzi
