// libreria
#include <iostream>
using namespace std;

#ifndef PILA_H
#define PILA_H

class Pila {
	private:
        string n = "\0";

    public:
        Pila();
        Pila(string n);

        //set y get
        void set_valor(string n);
        string get_valor();


};
#endif
