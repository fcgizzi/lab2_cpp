/* Compilación: $ make
 * Ejecución: $ ./programa
 */
 // librerira
#include <iostream>
using namespace std;
// clase
#include "Pila.h"

// función imprime la pila
void imprimir(int max, Pila pila[], int tope){
	cout << "  Pila:" << endl;
	// ciclo imprime dependiendo del tamaño del tope
	for(int i = tope; i > -1; i--){
		cout << "\t [" << pila[i].get_valor() << "]" << endl;
	}
}

// función encargada de eliminar datos de la pila
void remover_pop(Pila pila[], int &tope){
	// variable booleano para determinar si está llena o no la pila
	bool band;
	if(tope == -1){
		band = true;
		cout << "  >> La pila está vacía, primero introducir datos" << endl;
	}
	else{
		band = false;
		cout << "  >> Ultimo elemento eliminado: ["  << pila[tope].get_valor() << "]" << endl;
		tope = tope - 1;
	}
}

// funicón que se encarga de añadir valores a la pila
void agregar_push(int max, Pila pila[], int &tope, string n){
	
	Pila data = Pila();
	// variable booleano para determinar si está llena o no la pila
	bool band;
	if(tope+1 == max){
		band = true;
		// pila llena, band es true
		cout << ">> La pila está llena, no se pueden agregar más datos" << endl;
	}
	else{
		band = false;
		// pila aún no llena, agrega dato
		data.set_valor(n);
		tope = tope + 1;
		pila[tope] = n;
	}
}

// función menú, para opciones
void menu(int max, Pila pila[], int tope){
	int op;
	cout << "\t ---------------------" << endl;
	cout << "\t | 1 - Agregar/push  |" << endl;
	cout << "\t | 2 - Remover/pop   |" << endl;
	cout << "\t | 3 - Ver pila      |" << endl;
	cout << "\t | otro - Salir      |" << endl;
	cout << "\t ---------------------" << endl;
	cout << ">> ";
	cin >> op;
	
	// dependiendo de la opción cumplirá
	// su función correspondiente
	if(op == 1){
		string n;
		cout << ">> Ingresar valor: ";
		cin >> n;
		agregar_push(max, pila, tope, n);
		menu(max, pila, tope);
	}
	else if(op == 2){
		remover_pop(pila, tope);
		menu(max, pila, tope);
	}
	else if(op == 3){
		imprimir(max, pila, tope);
		menu(max, pila, tope);
	}
	else{
		cout << "|| ADIOS ||" << endl;
	}
}

// función principal
int main(int argc, char **argv){
	// variables máxima y de tope
	int max;
	int tope = -1;
	cout << ">> Cantidad máxima de la pila: ";
	cin >> max;
	
	// se define la pila con su maximo
	Pila pila[max];
	cout << ">> Ingresar opción para:" << endl;
	menu(max, pila, tope);

	return 0;
}
